// Declaración de variables del servidor
//   app define el framework
//   port define el puerto
var express = require ('express');
var app = express();
var port = process.env.PORT || 3000;

// Declaracion del body parser para ser usado por el framework
var bodyParser = require('body-parser');
app.use(bodyParser.json());


// Se pone el servidor a escuchar
app.listen(port);
console.log("API prueba escuchando en el puerto " + port);


// Se define funcion que escribe datos a archivo
function writeUserDataToFile (data) {
  // Se añade el registro a un nuevo fichero
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data, undefined, 2);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err) {
      if (err) {
        var msg = "[writeUserDataToFile] Error al escribir en fichero usuarios";
        console.log(msg);
      } else {
        var msg = "[writeUserDataToFile] Fichero usuarios escrito correctamente";
        console.log(msg);
      }
    }
  );
}


// Se define método get de prueba para la app, con la ruta en la que responde
// Se define la ruta y la función que se llama cuando llega la petición
//    get a la app
//    La función es la que da la respuesta (objeto res)
app.get("/apitechu/v1",
  function(req, res) {
    console.log("GET /apitechu/v1 :antes de la respuesta");
    //Respuesta HTML
    //res.send("Respuesta a la petición desde API TechU");

    //Respuesta JSON
    //res.send({"msg" : "Respuesta JSON desde API TechU"});
    res.send({"msg" : "Respuesta JSON desde API TechU"});
    console.log("GET /apitechu/v1 :despues de la respuesta");
  }
);


// Se define el método GET
// Va a devolver una lista de usuarios
// Se usa mockaroo para generar los datos aleatoriamente
// Se indica el fichero con el path referenciado con la variable __dirname
//   __dirname representa el directorio donde se está ejecutando
// Se devuelve un archivo, por eso se usa sendFile
app.get("/apitechu/v1/users",
  function(req, res) {
    console.log("GET /apitechu/v1/users");

    // Se responde
    res.sendFile('usuarios.json', {root: __dirname});
  }
);


// Se define el método POST
app.post("/apitechu/v1/users",
  function(req, res) {
    console.log("POST /apitechu/v1/users");
    console.log("first_name is " + req.body.first_name);
    console.log("last_name is " + req.body.last_name);
    console.log("country is " + req.body.country);

    // Se declara la variable para el nuevo usuario
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    // Se añade el nuevo usuario al listado de usuarios, llamando a la función
    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);
    msg = "Usuario añadido con éxito";
    console.log(msg);

    // Se responde
    res.send({"msg" : msg});
  }
);


// Se define el método DELETE
// Borra un elemento aleatorio, de manera poco optima, porque escribe todo
app.delete("/apitechu/v1/users/:id",
  function(req, res) {
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params.id);

    var users = require('./usuarios.json');
    // Método splice: mismo array sin el trozo que se quita
    users.splice(req.params.id-1, 1);

    // Se escribe el fichero
    writeUserDataToFile(users);
    msg = "Usuario borrado"
    console.log(msg);

    // Se responde
    res.send({"msg" : msg});
  }
);


// Se define metodo POST para probar paso de parámetros
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parametros");
    console.log(req.params);

    console.log("Query string");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
);


// ******************************************************
//  CONTENIDO DE LA PRACTICA DEL 25/04/2018
// ******************************************************

//
// PRACTICA 25/04/2018:
//   Se define el método GET
//   Va a devolver una lista de usuarios
//
app.get("/apitechu/v1/login",
  function(req, res) {
    console.log("GET /apitechu/v1/login");
    // Se responde
    res.sendFile('usuarios.json', {root: __dirname});
  }
);


//
// PRACTICA 25/04/2018:
//   Se define el método POST de login
//   Va a realizar el login
//
app.post("/apitechu/v1/login",
  function(req, res) {
    console.log("POST /apitechu/v1/login");
    console.log("email is " + req.body.email);
    console.log("password is " + req.body.password);

    // Se lee el fichero de usuarios
    var users = require('./usuarios.json');
    var userLogged = false;
    var idUser = -1;
    for (user of users)
    {
      if ((user.email == req.body.email) && (user.password == req.body.password))
        {
          userLogged = true;
          idUser = user.id;
          user.logged = true;
        }
    }

    if (userLogged == true)
    {
      msg = "LOGIN correcto";
      console.log(msg);
      // Se actualiza propiedad user.logged
      writeUserDataToFile(users);
      // Se responde
      res.send({"msg" : msg, "idUser": idUser});
    }
    else
    {
      msg = "LOGIN incorrecto";
      console.log(msg);
      // Se responde
      res.send({"msg" : msg});
    }
  }
);


//
// PRACTICA 25/04/2018:
//   Se define el método POST de logout
//   Va a realizar el logout
//
app.post("/apitechu/v1/logout",
  function(req, res) {
    console.log("POST /apitechu/v1/logout");
    console.log("idUser de entrada: " + req.body.id);

    // Se lee el fichero de usuarios
    var usuEncontrado = false;
    var idUser = -1;
    var users = require('./usuarios.json');
    for (user of users)
    {
      if ((user.id == req.body.id) && (user.logged == true))
        {
          usuEncontrado = true;
          idUser = user.id;
          delete user.logged;
          // Se actualiza propiedad user.logged
          writeUserDataToFile(users);
          break;
        }
    }
    if (usuEncontrado == true)
    {
      msg = "LOGOUT correcto";
      console.log(msg);
      // Se responde
      res.send({"msg" : msg, "idUser": idUser});
    }
    else
    {
      idUser = req.body.id;
      msg = "LOGOUT incorrecto";
      console.log(msg);
      // Se responde
      res.send({"msg" : msg, "idUser": idUser});
    }
  }
);
